/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.ui;

import JourneyThroughEurope.Game.AI;
import JourneyThroughEurope.Game.Card;
import JourneyThroughEurope.Game.City;
import JourneyThroughEurope.Game.Human;
import JourneyThroughEurope.Game.Player;
import JourneyThroughEurope.Game.Position;
import application.Main;
import application.Main.JTEPropertyType;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.image.Image;
import properties_manager.PropertiesManager;

/**
 *
 * @author Jason
 */
public class GameFileManager {
    
    private JourneyThroughEuropeUI ui;
    
    private String ImgPath = "file:images/";
    private String csvFilePath = "./data/cities.txt";
    private String neighborsFilePath = "./data/neighbors.txt";
    private String tabSplit = ",";
    private String saveGamePath = "./data/gameData.txt";
    
    private File file;
    private PrintWriter output;
    
    
    public GameFileManager(JourneyThroughEuropeUI initUI)
    {
        ui = initUI;
    }
    
    public Image loadImage(String imageName) {   
        Image img = new Image(ImgPath + imageName);
        return img;
  
    }
     public Image loadImage(String imageName, int length, int width) {
         Image img = new Image(ImgPath + imageName, length, width, true, true);
        return img;
                
     }
     public void saveGame()
     {
        try {
            System.out.println("FILE SAVED");
            output = new PrintWriter(saveGamePath);
            output.println(ui.getGSM().getCurrentPlayerTurn());// + ","+
                   // ui.getGSM().getAllPlayers().size());// saves the currentplayerTurn
            // now save the players and their objects
            for(Player player : ui.getGSM().getAllPlayers())
            {
               output.println(player.printFormat());
            }
            output.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GameFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
     public ArrayList<City> readFromCSVFile()
     {   
         ArrayList<City> cities = new ArrayList();
         
        try {
            BufferedReader br =new BufferedReader(new FileReader(csvFilePath));
            String inputLine;
            br.readLine();
            while((inputLine=br.readLine())!=null)
            {
                // city, Color, quadrant, xPos, y
                String[] options = inputLine.split(tabSplit);
                
                String name = options[0];
                String color = options[1];
                
                int quad = Integer.parseInt(options[2]);
                //0.25, 0.24
                int xPos = Integer.parseInt(options[3]);
                int yPos = Integer.parseInt(options[4]);
                xPos*=0.25;
                yPos*=0.24;
                Position pos = new Position(xPos,yPos);
 
               /* for(String str: options)
                {
                    System.out.print(" " + str);
                }
                System.out.println("");
                //System.out.println(inputLine);*/
                if(options.length < 6)
                    cities.add(new City(name,color,quad,pos));
                else
                {
                    xPos = Integer.parseInt(options[5]);
                    yPos = Integer.parseInt(options[6]);
                    xPos*=.48;
                    yPos*=.48;
                    Position flightPos = new Position(xPos,yPos);
                    int flightQuad = Integer.parseInt(options[7]);
                    cities.add(new City(name,color,quad,pos,flightPos,flightQuad));
                }
            }
        }catch (IOException ex) {
            Logger.getLogger(GameFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         return cities;
         
     }

    public void initNeighbors(ArrayList<City> cities) {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> cityNeighbors =
                props.getPropertyOptionsList(JTEPropertyType.CITY_NEIGHBORS);
        int index = 0;
        for(String neighbor: cityNeighbors)
        {
            //System.out.println("cityNeighbors = " + cityNeighbors.get(index));
            String[] options = cityNeighbors.get(index++).split(tabSplit);
            City city = ui.getGSM().findCity(options[0]);
            //System.out.println("city = " + city.getName() + " " + index);
            for(int i = 1;i < options.length;i++)
             {
                if(options[i].contains("$"))//this means searoute is true
                {
                    
                    String newCity = options[i].substring(options[i].indexOf("$")+1);// substring from the rest of the city
                    City cityNeighbor = ui.getGSM().findCity(newCity);
                    if(cityNeighbor == null)
                        System.out.println("mainCity " + city.getName() + " neighbor: "
                           + newCity);
                    else
                        city.addNeighbor(cityNeighbor, true);// means the route is a searoute
                } 
                else
                {
                    City cityNeighbor = ui.getGSM().findCity(options[i]);
                    city.addNeighbor(cityNeighbor, false);
                }
             }
        }
        
    }

    public void loadGame() {
        //initialize cities and deck
        ui.getGSM().loadNewGame();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> playerPieceImgName = 
                props.getPropertyOptionsList(JTEPropertyType.PLAYERS_PIECES);               
        ArrayList<String> flagImgNames =
                props.getPropertyOptionsList(JTEPropertyType.FLAG_NAMES);
        try {
            BufferedReader br =new BufferedReader(new FileReader(saveGamePath));
            String inputLine = br.readLine();
            // sets the currentPlayer Turn
            ui.getGSM().setCurrentPlayerTurn(Integer.parseInt(inputLine));
            int index = 0;
            ArrayList<Player> players = new ArrayList();
            
            while((inputLine=br.readLine())!=null)
            {
       // Player(Name, Color,PlayerPieceImg,PlayerFlagImg)
                Image PlayerPieceImg = loadImage(playerPieceImgName.get(index),40,40);
                Image playerFlagImg = loadImage(flagImgNames.get(index),40,40);
                String[] options = inputLine.split(tabSplit);
                String name = options[0];
                String color = options[1];
                int points = Integer.parseInt(options[2]);
                boolean isHuman = Boolean.parseBoolean(options[3]);
                //origin,next,current,previous
                if(isHuman)
                    players.add(new Human(name,color,PlayerPieceImg,playerFlagImg));
                else
                    players.add(new AI(name,color,PlayerPieceImg,playerFlagImg));
                System.out.println("Players are created");
                City origin = ui.getGSM().findCity(options[4]);
                City next = ui.getGSM().findCity(options[5]);
                City current = ui.getGSM().findCity(options[6]);
                City previous = ui.getGSM().findCity(options[7]);
                
                players.get(index).setHomeCity(origin);
                players.get(index).setCurrentDestination(next);
                players.get(index).setCurrentCity(current);
                players.get(index).setPreviousCity(previous);
                System.out.println("Cities found and set");
                //next are the Cards
                for(int i = 8; i < options.length-2;)
                {
                int key = Integer.parseInt(options[i]);
                Card value = ui.getGSM().findCardfromDecks(options[i+1],options[i+2]);
                players.get(index).giveCard(key, value);
                i+=3;
                }
                // all players were initilze
                System.out.println("Cards were placed in their hashes");
                index++;
               
                
            }
             ui.getGSM().setPlayers(players);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GameFileManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch(IOException ex)
        {
            
        }
    }
    
}
