/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.ui;

import JourneyThroughEurope.Game.Player;
import JourneyThroughEurope.Game.Position;
import application.Main.JTEPropertyType;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;

/**
 *
 * @author Jason
 */
public class JTEeventHandler {
    
    private JourneyThroughEuropeUI ui;
    
    public JTEeventHandler(JourneyThroughEuropeUI initUI)
    {
        ui = initUI;
    }

    public void respondtoStartGameRequest() {
        //ui.getMainPane()
        ui.initPlayerSelectionScreen(2);// starts the game off with 2 players
        
    }
    public void respondtoBeginGameRequest()
    {
        ui.getGSM().startNewGame();
        ui.inItGameScreen();
        
        
    }
        
    public void respondtoLoadGameRequest() {
        ui.getGFM().loadGame();
        ui.getMainPane().getChildren().clear();
        ui.getMainPane().setStyle("-fx-background-color: #FAEDDB;");//"-fx-background-color : #d3d3d3;");
        ui.ClearCanvas();
        ui.update();
    }
    
    public void respondtoSaveGameRequest() {
        ui.getGFM().saveGame();
    }
    
    public void respondtoFlightRequest() {
        ui.displayFlightCard();
    }
    
    public void respondtoExitGameRequest() {
        
            // YES, LET'S EXIT
            System.exit(0);
    }
    
    public void respondtoMove(Player player)
    {
        
    }
    public void respondtoPlayerSelection()
    {
        
    }
    
    public void repsondtoSelectPlayers(int number)
    {
        
    }
    
    public void repsondtoQuadrantClick(int quad, JTEPropertyType jteState)
    {   ui.getBoardRenderer().setjteState(jteState);
        ui.getBoardRenderer().repaint(quad-1);
    }
    public void repsondtoQuadrantClick(int quad)
    {  // ui.getBoardRenderer().setjteState(JTEPropertyType.QUADRANT1);
        ui.getBoardRenderer().repaint(quad-1);
        ui.setCurrentQuadrant(quad);
    }

    public void respondtoAboutGameRequest() {
        
           if(Desktop.isDesktopSupported())
                {
               try {
                   try {
                       Desktop.getDesktop().browse(new URI("http://en.wikipedia.org/wiki/Journey_Through_Europe"));
                   } catch (IOException ex) {
                       Logger.getLogger(JTEeventHandler.class.getName()).log(Level.SEVERE, null, ex);
                   }
               } catch (URISyntaxException ex) {
                   Logger.getLogger(JTEeventHandler.class.getName()).log(Level.SEVERE, null, ex);
               }
                }
        
    }

    public void respondtoOpenHistory() {
        // FIRST MAKE SURE THE USER REALLY WANTS TO EXIT
        ui.itItHistoryScreen();
        /*Stage dialogStage = new Stage();
        Button yesButton = new Button("CLOSE");
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(e -> {
            // YES, LET'S EXIT
            dialogStage.hide();
        });
        
        Label historyLabel = new Label("GAME HISTORY");
        
        BorderPane historyPane = new BorderPane();
        
        HBox labelBox = new HBox();
        
        labelBox.setAlignment(Pos.CENTER);
        labelBox.getChildren().add(historyLabel);
        
        HBox ButtonPane = new HBox();
        ButtonPane.setAlignment(Pos.CENTER);
        ButtonPane.getChildren().add(yesButton);
        
        historyPane.setTop(labelBox);
        historyPane.setBottom(ButtonPane);
        
        
        
        Scene scene = new Scene(historyPane, 250, 100);
        
        dialogStage.setScene(scene);
        dialogStage.show();*/
    }
    
    public void respondtoFlightMove(Position position) {
        ui.getGSM().flightMove(position);
    }

    public void displayNoFlightMessage() {
        Stage dialogStage = new Stage();
        BorderPane flightMessagePane = new BorderPane();
        
        Label flightMessage = new Label();
        flightMessage.setText(ui.getGSM().getCurrentPlayer().getName() + " " + 
                "is not on a FlightCity");
        
        Button closeButton = new Button("CLOSE");
        closeButton.setAlignment(Pos.CENTER);
        closeButton.setOnAction( e ->{
            dialogStage.hide();
        });
        
        flightMessagePane.setCenter(flightMessage);
        flightMessagePane.setBottom(closeButton);
        Scene scene = new Scene(flightMessagePane, 250, 100);
        
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    public void respondtoDisplayTowninformaiton() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> srsInfo = 
                props.getPropertyOptionsList(JTEPropertyType.TOWN_INFORMATION);
        Stage dialogStage = new Stage();
        Button yesButton = new Button("CLOSE");
        // WHAT'S THE USER'S DECISION?
        yesButton.setOnAction(e -> {
            // YES, LET'S EXIT
            dialogStage.hide();
        });
        
        Label townLabel = new Label("Town Information");
        
        //BorderPane historyPane = new BorderPane();
        ScrollPane sp = new ScrollPane();
        
        sp.setContent(new Label(srsInfo.get(0)));
        sp.setVbarPolicy(ScrollBarPolicy.NEVER);
        
        VBox srsPane = new VBox();
        srsPane.getChildren().addAll(townLabel,sp);
        
        
        
        Scene scene = new Scene(srsPane, 500, 750);
        
        dialogStage.setScene(scene);
        dialogStage.show();
    }

    public void respondtoDrag(Position pos) {
        //ui.ClearCanvas();
        //System.out.println("pos = " + pos);
        ui.getBoardRenderer().drawDrag(pos);
    }

}
