package JourneyThroughEurope.ui;

import JourneyThroughEurope.Game.AI;
import JourneyThroughEurope.Game.Card;
import JourneyThroughEurope.Game.GameStateManager;
import JourneyThroughEurope.Game.Human;
import JourneyThroughEurope.Game.Player;
import JourneyThroughEurope.Game.Position;
import application.Main.JTEPropertyType;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javafx.animation.ParallelTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.util.Duration;
import properties_manager.PropertiesManager;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Jason Fontaine
 */
public class JourneyThroughEuropeUI {
    
    private GameStateManager gsm;
    private GameFileManager gfm;
    private JTEeventHandler eventHandler;
 
    private Button startButton = new Button(),
                   loadButton = new Button(),
                   aboutButton = new Button(),
                   quitButton = new Button(),
                   flightButton = new Button(),
                   startGameButton = new Button("GO"),
                   historyGameButton = new Button(),
                   saveButton = new Button(),
                   srsButton = new Button("Town Information");
    
   // private RadioButton playerRadio = new RadioButton("Player"),
                        //computerRadio = new RadioButton("Computer");
    
    private BorderPane mainPane,
                       historyPane;
    
    private int paneWidth;
    private int paneHeigth;
    private int currentQuad;
    //private int currentPlayer = 0;
    private double animationLength = 0.5;
    
    
    private StackPane splashScreen;
    
    private VBox buttonToolbar,
                 cardToolbar,
                 rightToolbar,
                 leftPane;
                 
    private HBox northToolbar;
    
    private Pane diePane;
    
    private GridPane playerSelectionToolbar = new GridPane(),
                     quadrantsGridPane;
    
    private ScrollPane historyScrollPane;
    
    private Image flightCardImage;
    private ImageView splashScreenImageView;
    
    private ImageView currentPlayerImg;
    
    private ImageView dieImgView;
    
    private Label dieLabel = new Label();
    
    private Insets marginlessInsets;
    
    private Stage primaryStage;
    
    private BoardRenderer boardRenderer;
    
    private ChoiceBox playersPerGame;
    
    private Image quadrant1,
                  quadrant2,
                  quadrant3,
                  quadrant4;
    
    private boolean hasCardRemoved = false;
    private Label splashScreenImageLabel,
                  playersLabel = new Label("Number of Players"),
                  currentPlayerLabel = new Label(),
                  currentPlayerPieceLabel = new Label(),
                  cityLabel = new Label("Select City"),
                  diceRolledLabel = new Label(),
                  currentPlayerCards = new Label();
                //  nameLabel = new Label("Name: ");
    
    private ArrayList<String> flagImageNames,
                              quadrantsImgNames,
                              playerNames;
    
    private ArrayList<TextField> textField;
    private ArrayList<Card> currentCards;
    
   private HashMap<Integer, Boolean> isPlayerHuman = new HashMap();
    
   // final ArrayList<JTEPropertyType> jteState;
    
    
    public JourneyThroughEuropeUI(Stage primaryStage) {
        setPrimaryStage(primaryStage);
        gsm = new GameStateManager(this);
        eventHandler = new JTEeventHandler(this);
        gfm = new GameFileManager(this);
        boardRenderer = new BoardRenderer(this,gsm);
        currentCards = new ArrayList();
        
        
    }

    public void StartUI() {
        mainPane = new BorderPane();
        historyPane = new BorderPane();
        historyScrollPane = new ScrollPane();
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        props.addProperty(JTEPropertyType.INSETS, "5");
        paneWidth = Integer.parseInt(props
                .getProperty(JTEPropertyType.WINDOW_WIDTH));
        paneHeigth = Integer.parseInt(props
                .getProperty(JTEPropertyType.WINDOW_HEIGHT));
        mainPane.resize(paneHeigth, paneHeigth);
        int insetsvalue = Integer.parseInt(props
                .getProperty(JTEPropertyType.INSETS));
        marginlessInsets = new Insets(insetsvalue);
        mainPane.setPadding(marginlessInsets);
        initSplashScreen();
        
    }
    
    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }
    
    public BoardRenderer getBoardRenderer()
    {
        return boardRenderer;
    }

    public void initSplashScreen() {
        
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String splashScreenImagePath = props.getProperty(JTEPropertyType.SPLASH_SCREEN_BOARD);
        
        splashScreen = new StackPane();
        
        Image splashScreenImage;
        splashScreenImage = new Image("file:images/Game.JPG",600,1250, true, true);
        //loadImage(splashScreenImagePath);
        
        splashScreenImageView = new ImageView(splashScreenImage);
        
        
        splashScreenImageLabel = new Label();
        splashScreenImageLabel.setGraphic(splashScreenImageView);
        splashScreen.setAlignment(Pos.TOP_CENTER);
        splashScreen.getChildren().add(splashScreenImageLabel);
        
        // initialize Buttons for the screen
        ArrayList<String> levelImagesNames = props.getPropertyOptionsList(JTEPropertyType.SPLASHSCREEN_BUTTONS);
        
        buttonToolbar = new VBox();
        buttonToolbar.setAlignment(Pos.BOTTOM_CENTER);
        
        Image buttonImage = gfm.loadImage(levelImagesNames.get(0));
        ImageView buttonView = new ImageView(buttonImage);
        
        
        startButton.setGraphic(buttonView);
        buttonToolbar.getChildren().add(startButton);
        
        buttonImage = gfm.loadImage(levelImagesNames.get(1));
        buttonView = new ImageView(buttonImage);
        
        loadButton.setGraphic(buttonView);
        buttonToolbar.getChildren().add(loadButton);
        
        buttonImage = gfm.loadImage(levelImagesNames.get(2));
        buttonView = new ImageView(buttonImage);
        
        aboutButton.setGraphic(buttonView);
        buttonToolbar.getChildren().add(aboutButton);
        
        buttonImage = gfm.loadImage(levelImagesNames.get(3));
        buttonView = new ImageView(buttonImage);
        
        quitButton.setGraphic(buttonView);
        buttonToolbar.getChildren().add(quitButton);
        
        startButton.setOnAction(e ->{
                eventHandler.respondtoStartGameRequest();
        });
        
        loadButton.setOnAction(e ->
        {
            eventHandler.respondtoLoadGameRequest();
        });
        
        aboutButton.setOnAction(e ->{
            eventHandler.respondtoAboutGameRequest();
        });
        
        quitButton.setOnAction( e ->{
            eventHandler.respondtoExitGameRequest();
        });
    splashScreen.getChildren().add(buttonToolbar);
       
    
    mainPane.setStyle("-fx-background-image: url(\"file:images/waves.GIF\");"
            + " -fx-background-repeat: no-repeat;"
            + " -fx-background-size: cover;"
            + " -fx-background-position: center center;");
     mainPane.setCenter(splashScreen);
     //mainPane.setBottom(buttonToolbar);
        
    }
    
    public void initPlayerSelectionScreen(int numberOfPlayers)
    {
        Integer two=2,three=3,four=4,five=5,six=6;
        
        // set Up flag images
       PropertiesManager props = PropertiesManager.getPropertiesManager();
       
       flagImageNames = props.getPropertyOptionsList(JTEPropertyType.FLAG_NAMES);
        
        northToolbar = new HBox();
        northToolbar.setSpacing(10);
        playersPerGame = new ChoiceBox(FXCollections.observableArrayList(
        two,three,four,five,six));
        
        playersPerGame.setTooltip(new Tooltip("Select the number of Players"));
        
        playersPerGame.getSelectionModel().select(numberOfPlayers-2);
        
        initPlayerSelectionPane(2);
        
        playersPerGame.valueProperty().addListener(new ChangeListener<Integer>()
        {

            @Override
            public void changed(ObservableValue<? extends Integer> observable, Integer oldValue, Integer newValue) {
               // System.out.println("newValue = " + newValue);/// the value you want to use to build the panes
               // playerSelectionToolbar = new GridPane();
                playerSelectionToolbar.getChildren().clear();
                isPlayerHuman = new HashMap();
                initPlayerSelectionPane(newValue);
                  //playerSlectionToolbar.add(levelButton, i % 3, i / 3);
                
            }

           
         
        }
        );
        playerSelectionToolbar.setAlignment(Pos.CENTER);
        northToolbar.getChildren().addAll(playersLabel,playersPerGame,startGameButton);
        mainPane.setCenter(playerSelectionToolbar);
        mainPane.setStyle("-fx-background-color: #FAEDDB;");//"-fx-background-color : #d3d3d3;");
        mainPane.setTop(northToolbar);
           
    }
    
    public void initPlayerSelectionPane(int end)
    {
        int xvalue =0,yvalue =0;
                //Integer playerNumber = 0;
        playerNames = new ArrayList();
        textField = new ArrayList();
                for(int i = 0; i < end; i++)
                {
                    ToggleGroup group = new ToggleGroup();
                    VBox radioButtonVbox = new VBox();
                    VBox nameVbox = new VBox();
                    HBox collectionsPane = new HBox();
                    collectionsPane.setAlignment(Pos.CENTER);
                    collectionsPane.setSpacing(10);
                    Pane flagPosition = new Pane();
                    
                    final int playerNumber = i;
                    
                    RadioButton playerRadio = new RadioButton("Human"),
                                computerRadio = new RadioButton("Computer");
                    
                    playerRadio.setToggleGroup(group);
                    playerRadio.setSelected(true);
                    if(playerRadio.isSelected())
                        isPlayerHuman.put(playerNumber, true);
                    
                    playerRadio.setOnMouseClicked(e->{
                       // computerRadio.setDisable(true);// sets the isPlayerHuman to true
                        
                        isPlayerHuman.put(playerNumber, true);
                    });
                    
                    computerRadio.setToggleGroup(group);
                    computerRadio.setOnAction( e->{
                        //playerRadio.setDisable(true);
                        // sets the isPlayerHuman to true// sets the isPlayerHuman to false
                        isPlayerHuman.put(playerNumber, false);
                    });
                    
                   // playerRadio.disarm();
                    
                    Label nameLabel = new Label("Name: ");
                    
                    // adds Flag to Pane
                    Image flagImg = gfm.loadImage(flagImageNames.get(i),100,100);
                    
                    ImageView flagImgView = new ImageView(flagImg);
                    Label flagLabel = new Label();
                    flagLabel.setGraphic(flagImgView);
                    
                    // add radio buttons to pane
                    radioButtonVbox.getChildren().addAll(playerRadio, computerRadio);
                    
                    //add textField to Pane
                     textField.add(new TextField("Player " + (i+1)));
                    textField.get(i).setPrefWidth(100);
                    textField.get(i).setOnAction( e ->{

                    });
                    
                    nameVbox.getChildren().addAll(nameLabel, textField.get(i));
                    
                    
                    flagPosition.getChildren().add(flagLabel);// adds to the pane
                    
                    // add the nodes the the collections pane
                    collectionsPane.getChildren().addAll(flagPosition,radioButtonVbox,nameVbox);
                    collectionsPane.setStyle("-fx-border-style: solid;"
                                           + " -fx-border-color: red");
                            
                    // add collections to the gridPane
                    playerSelectionToolbar.add(collectionsPane, xvalue, yvalue++);
                    if(yvalue> 2){
                        xvalue++;
                        yvalue = 0;
                    }
                }
                
                startGameButton.setOnAction(e ->
                    {
                        if(!isPlayerHuman.isEmpty()){
                        for(TextField txt: textField){
                           playerNames.add(txt.getText());
                        }
                        gsm.initPlayers(isPlayerHuman, playerNames);
                        eventHandler.respondtoBeginGameRequest();
                        }System.out.println("Really");
                        
                    }
                    );
    }
   
    public BorderPane getMainPane()
    {
        return mainPane;
    }
    public GameFileManager getGFM()
    {
        return gfm;
    }
    public int getpaneWidth()
    {
        return paneWidth;
    }
    
    public int getpaneHeight()
    {
        return paneHeigth;
    }

    public void inItGameScreen() {
        mainPane.getChildren().clear();
        inItRightToolbar();// Display Dice, quadrants, history
        inItBoard();// the board renderer
        inItLeftToolbar();// display cards on left handside
        if(gsm.getCurrentPlayer() instanceof AI)// if computer has first move
            gsm.processComputerMove();
        
    }
    public void sethasCardRemove(boolean value)
    {
        hasCardRemoved = value;
    }
    public void inItRightToolbar() {
       PropertiesManager props = PropertiesManager.getPropertiesManager();
       
       
       rightToolbar = new VBox();
       rightToolbar.setAlignment(Pos.TOP_CENTER);
       rightToolbar.setSpacing(10);
       
       diePane = new Pane();
       
       quadrantsGridPane = new GridPane();
       
       HBox currentPlayerPane = new HBox();
       currentPlayerPane.setAlignment(Pos.CENTER);
       
       setCurrentPlayertoScreen();

       currentPlayerPane.getChildren().addAll(currentPlayerLabel,currentPlayerPieceLabel);
       
       ///
       ArrayList<String> dieImgNames = 
               props.getPropertyOptionsList(JTEPropertyType.DICE_IMG_NAMES);
       // add methof about setting points here
       int roll = gsm.roll();
       if(!hasCardRemoved)
            gsm.setPlayerPoints(roll+1);
       setDiceRollLabel(roll);
       Image dieImg = gfm.loadImage(dieImgNames.get(roll));
       dieImgView = new ImageView(dieImg);
       dieLabel = new Label();
       dieLabel.setGraphic(dieImgView);
       diePane.getChildren().add(dieLabel);
               
       int xValue = 0, yValue = 0;
       for(int i = 0;i < 4; i++)
       {
           int quad = i + 1;
           Button quadButton = new Button("quadrant " + (quad));
           
           quadButton.setOnAction(e -> {
               eventHandler.repsondtoQuadrantClick(quad);
           });
           
           quadrantsGridPane.add(quadButton, xValue++, yValue);
           if(xValue > 1)
           {
               xValue = 0;
               yValue++;
           }
       }
       // make flight plan button
       String flightPlanImgName = props.getProperty(JTEPropertyType.FLIGHT_PLAN_BUTTON);
       
       Image flightPlanImg = gfm.loadImage(flightPlanImgName);
       ImageView flightPlanImgView = new ImageView(flightPlanImg);
       
       flightButton.setGraphic(flightPlanImgView);
       
       flightButton.setOnAction( e -> {
           if(gsm.getCurrentPlayer().getCurrentCity().isAirPort())
                eventHandler.respondtoFlightRequest();
           else
               eventHandler.displayNoFlightMessage();
       });
       
       // make History Button
       String historyImgName = props.getProperty(JTEPropertyType.HISTORY_GAME_BUTTON);
       
       Image historyImg = gfm.loadImage(historyImgName);
       ImageView historyImgView = new ImageView(historyImg);
       
       historyGameButton.setGraphic(historyImgView);
       
       historyGameButton.setOnAction( e ->{
           eventHandler.respondtoOpenHistory();
       });
       
      // make Save Button
       String saveImgName = props.getProperty(JTEPropertyType.SAVE_BUTTON);
       
       Image saveImg = gfm.loadImage(saveImgName);
       ImageView saveImgView = new ImageView(saveImg);
       
       saveButton.setGraphic(saveImgView);
       
       saveButton.setOnAction( e ->{
          // if(!gsm.isHistoryEmpty())// will not 
                eventHandler.respondtoSaveGameRequest();
       });
       
       srsButton.setOnAction( e -> {
           eventHandler.respondtoDisplayTowninformaiton();
       });
       
       rightToolbar.getChildren().addAll(currentPlayerPane,diceRolledLabel,
                                        cityLabel,dieImgView,quadrantsGridPane,
                                        flightButton,aboutButton,
                                        historyGameButton,saveButton,srsButton);
       rightToolbar.setStyle("-fx-background-color: #29A3CC");
       
       mainPane.setRight(rightToolbar);
       
       
       
    }

    public void inItBoard() {
        // load images
       
        int quad = 1;
        ClearCanvas();
        
        eventHandler.repsondtoQuadrantClick(gsm.getCurrentPlayer().getCurrentCity().getQuadrant());
       
        
        
        
    }
       public void ClearCanvas() {
      //  mainPane.setCenter(null);
        //boardRenderer
         
         //boardRenderer.set
       // boardRenderer = null;
        
        mainPane.setCenter(null);
        boardRenderer.setOnMouseClicked(e ->{
            //if(boardRenderer.getjtePropertytype() == 
            //        JTEPropertyType.QUADRANT1)
           // {
                //System.out.println(e.getX() +"\t"+e.getY());
                if(gsm.getCurrentPlayer() instanceof Human)
                {
                gsm.findCitiesInQuad(currentQuad, new Position((int)e.getX(),(
                                        int)e.getY()));
                if(gsm.getCurrentPlayer().canMove())
                    gsm.movePlayertoCity();
                
                if(gsm.getCurrentPlayer().isTurnOver()) 
                    //if(!gsm.gameStillinProgress())
                        {
                           int points = gsm.getCurrentPlayer().getPoints();
                           gsm.getCurrentPlayer().addPoints(-points);
                           if(gsm.getCurrentPlayer().getOriginPoints() != 6)    
                               gsm.nextTurn();
                           this.sethasCardRemove(false);
                           update();
                           if(gsm.getCurrentPlayer() instanceof AI)
                                gsm.processComputerMove();
                           //boardRenderer.repaint(gsm.getCurrentPlayer().getCurrentCity().getQuadrant()-1);
                        }
                    
                }
           // }
            
        }
                 
        );
        boardRenderer = new BoardRenderer(this,gsm);
       /* boardRenderer.addEventHandler(MouseEvent.MOUSE_DRAGGED, 
           new EventHandler<MouseEvent>() {
           @Override
           public void handle(MouseEvent e) {
               System.out.println("e = " + e.getX() + " " + e.getY());
               eventHandler.respondtoDrag(new Position(e.getX(),e.getY()));
               
           }
       });*/
        boardRenderer.setOnMouseDragged(e ->{
             System.out.println("e = " + e.getX() + " " + e.getY());
               eventHandler.respondtoDrag(new Position(e.getX(),e.getY()));
              
        });
        
        
    }
    public void inItLeftToolbar() {
       PropertiesManager props = PropertiesManager.getPropertiesManager();
       leftPane = new VBox();
      // AnchorPane anchorPane = new AnchorPane();
        
        setCurrentCardsLabel();
        StackPane currentPlayerCardPane = new StackPane();
        currentPlayerCardPane.setAlignment(Pos.CENTER);
        currentPlayerCardPane.getChildren().add(currentPlayerCards);
        currentPlayerCardPane.setStyle("-fx-border-style: solid;");
        leftPane.getChildren().add(currentPlayerCardPane);
        
        /*leftPane.setPadding(marginlessInsets);
        Card card = gsm.getaCard();
        Image cardImg = card.getFrontImg();
        ImageView cardImgView = new ImageView(cardImg);
        leftPane.getChildren().addAll(currentPlayerCardPane,cardImgView);
        
        Card card1 = gsm.getaCard();
        cardImg = card1.getFrontImg();
        cardImgView = new ImageView(cardImg);
        leftPane.getChildren().add(cardImgView);
        
        AnchorPane.setTopAnchor(leftPane, -1.0);*/
        HashMap<Integer,Card> currentPlayerCards = gsm.getCurrentPlayer().getCards();
        //ArrayList<ImageView> cardImages = new ArrayList();
        ParallelTransition parallelTransition = new ParallelTransition();
        int offSet = 0;
        for(int i =0;i<currentPlayerCards.size();i++)
        {
            ImageView cardImg = new 
                                    ImageView(currentPlayerCards.get(i).getFrontImg());
            currentCards.add(currentPlayerCards.get(i));
            if((cardImg!=null))
            {
                TranslateTransition translateTransition =
                new TranslateTransition(Duration.millis(1000), cardImg);
                translateTransition.setFromX(paneWidth/2);
                translateTransition.setFromY(paneHeigth/2);
                translateTransition.setToX(-1);
                translateTransition.setToY(15-offSet);

                offSet+=215;

                parallelTransition.getChildren().add(translateTransition);
                //cardImages.add(new ImageView(currentPlayerCards.get(i).getFrontImg()));
                leftPane.getChildren().add(cardImg);
                
                
            }
        }
        //Pane pane = (Pane) mainPane.getLeft();
        mainPane.setLeft(null);
        parallelTransition.play();
        mainPane.setLeft(leftPane);
        
        
        
        
        
        
    }
    
    public void setCurrentQuadrant(int quad) {
        currentQuad = quad;
    }

    public void setCityLabel(String name) {
        cityLabel.setText(name);
    }
    public void setCurrentCardsLabel()
    {
        currentPlayerCards.setAlignment(Pos.CENTER);
        currentPlayerCards.setText("Player " + (gsm.getCurrentPlayerTurn()+1));
    }
    public void setCurrentPlayertoScreen() {
        currentPlayerLabel
                .setText("Player " + (gsm.getCurrentPlayerTurn()+1)
                + " Turn");
       Image playPieceImg = gsm.getCurrentPlayerImg();
       ImageView playPieceImgView = new ImageView(playPieceImg);
       currentPlayerPieceLabel.setGraphic(playPieceImgView);
       
    }

    public void setDiceRollLabel(int roll) {
        diceRolledLabel.setText("Rolled " + (roll+1));
    }

    public GameStateManager getGSM() {
        return gsm;
    }

    public void update() {
    if(!gsm.gameStillinProgress())
    {
        inItLeftToolbar();
        inItRightToolbar();
        int quad = gsm.getCurrentPlayer().getCurrentCity().getQuadrant();
        eventHandler.repsondtoQuadrantClick(quad);
        
    }
    else // the game is over
        gsm.endGame();
    }

    public void displayFlightCard() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String flightCardImgName = props.getProperty(JTEPropertyType.FLIGHT_IMG_PATH);
        Image Img = gfm.loadImage(flightCardImgName);
        Image flightCardImg = gfm.loadImage(flightCardImgName,
                (int)(Img.getWidth()*.48),(int)(Img.getHeight()*.48));
        ClearCanvas();
        boardRenderer.drawFlightCard(flightCardImg);
        
        boardRenderer.setOnMouseClicked( e -> {
            
                eventHandler.respondtoFlightMove(new Position((int)e.getX(),(
                                        int)e.getY()));
           // System.out.println(e.getX() + " " + e.getY());
        });
        
        
    }
    
    public void updateDieImgView(int die) {
        if(die > 0)
        {
            PropertiesManager props = PropertiesManager.getPropertiesManager();
            ArrayList<String> dieImgNames = 
                   props.getPropertyOptionsList(JTEPropertyType.DICE_IMG_NAMES);
            Image img = gfm.loadImage(dieImgNames.get(die));
            dieImgView.setImage(img);
        }
       
       
    }
    
    public void removeCardfromLeftPane(Image cardImg) {
        leftPane.getChildren().remove(cardImg);
    }

    public void endGame() {
      PropertiesManager props = PropertiesManager.getPropertiesManager();
      VBox winnerPane = new VBox();
      String winnerImgName = props.getProperty(JTEPropertyType.WINNER);
      Image Img = gfm.loadImage(winnerImgName);
      Image winnerImg = gfm.loadImage(winnerImgName, (int)(Img.getWidth()*.05),
              (int)(Img.getHeight()*.05));
      ImageView winnerImgView = new ImageView(winnerImg);
      
      Label winnerLabel = new Label(gsm.getCurrentPlayer().getName() + 
              " has won the Game Congradulations!!!!");
      winnerPane.setAlignment(Pos.CENTER);
      winnerPane.getChildren().addAll(winnerImgView,winnerLabel);
      
      
      // loads image
      
      // set historySchooll pane to the center.
      /// set ImageView to the top
      mainPane.getChildren().clear();
      ClearCanvas();
      itItHistoryScreen();
      mainPane.setTop(winnerPane);
      //historyPane.setCenter(historyScrollPane);
      
      //mainPane.setCenter(historyPane);
      
    }

    public void itItHistoryScreen() {
        //FlowPane SRSpane = new FlowPane();
        Button returntoGame = new Button("RETURN TO GAME");
        returntoGame.setOnAction( e ->{
            boardRenderer.repaint(gsm.getCurrentPlayer().getCurrentCity().getQuadrant()-1);
            mainPane.setTop(null);
        });
        StackPane returnPane = new StackPane();
        returnPane.setAlignment(Pos.TOP_CENTER);
        returnPane.getChildren().add(returntoGame);
        
        VBox recordedHistory = new VBox();
        recordedHistory.setAlignment(Pos.TOP_CENTER);
        ArrayList<Player> players = gsm.getAllPlayers();
        for(Player player : players)
            for(String history : player.getHistory())
            {
                recordedHistory.getChildren().add(new Label(history));
            }
        
        //recordedHistory.getChildren().add(SRSpa);
        //mainPane.setTop(returnPane);
        //historyPane.setTop(returnPane);
       // historyScrollPane.setStyle("-fx-background-color: #FAEDDB;");
        historyScrollPane.setContent(recordedHistory);
        //historyScrollPane.setStyle("-fx-background-color: #FAEDDB;");
        //historyPane.setCenter(historyScrollPane);
        mainPane.setCenter(historyScrollPane);
    }

}
