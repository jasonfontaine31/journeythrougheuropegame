/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.ui;

import JourneyThroughEurope.Game.City;
import JourneyThroughEurope.Game.GameStateManager;
import JourneyThroughEurope.Game.Player;
import JourneyThroughEurope.Game.Position;
import application.Main;
import application.Main.JTEPropertyType;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javafx.animation.AnimationTimer;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import properties_manager.PropertiesManager;

/**
 *
 * @author Jason
 */
public class BoardRenderer extends Canvas{
    
    private JourneyThroughEuropeUI ui;
    private GraphicsContext gc;
    private ArrayList<String> quadrantsImgNames;
    private GameFileManager gfm;
    private GameStateManager gsm;
    private JTEPropertyType jteState;
    
    private int width = 800,
               height = 800;
    private static boolean isFirst;
    
    private StackPane pane;
    
    
    public BoardRenderer(JourneyThroughEuropeUI initUI, GameStateManager newGSM)
    {
        ui = initUI;
        gfm = new GameFileManager(initUI);
        gsm = newGSM;
        this.setWidth(515);
        this.setHeight(700);
        pane = new StackPane();
        isFirst = false;
    }

    public void repaint(int quad) {
        if(gc == null)
        {
            gc = this.getGraphicsContext2D();
            gc.setStroke(Color.RED);
        }
        //gc.clearRect(0, 0, this.getWidth(), this.getHeight());
        ui.ClearCanvas();// may need ot change method

        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
        quadrantsImgNames = props.getPropertyOptionsList(Main.JTEPropertyType.QUANDRANT_IMG_NAMES);
         
        Image newQuadImg = gfm.loadImage(quadrantsImgNames.get(quad));
        Image quadImg = gfm.loadImage(quadrantsImgNames.get(quad),
                                     ((int)(newQuadImg.getWidth()*.25)),
                                     ((int)(newQuadImg.getHeight()*.24)));
       //System.out.println("quadImg size = " + (quadImg.getWidth() + " " + quadImg.getHeight()));
       this.setWidth(quadImg.getWidth());
       this.setHeight(quadImg.getHeight());
        
        
       //if(!isFirst)
        {
           // gc.scale(0.25, 0.24);
            isFirst = true;
        }
        gc.drawImage(quadImg, 0, 0);
        
        
        drawPlayers(quad);
        
        
        //ui.getMainPane().setCenter(null);
        //ui.getMainPane().getChildren().remove(pane);
        pane.getChildren().add(this);
        pane.setAlignment(Pos.TOP_CENTER);
        ui.getMainPane().setCenter(pane);
        
    }
    public Canvas getCanvas()
    {
        return this;
    }
    public void repaintAnimation(int quad, Player player) {
            gc = this.getGraphicsContext2D();
            gc.setStroke(Color.RED);
        //gc.clearRect(0, 0, this.getWidth(), this.getHeight());
        ui.ClearCanvas();// may need ot change method

        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
        quadrantsImgNames = props.getPropertyOptionsList(Main.JTEPropertyType.QUANDRANT_IMG_NAMES);
         
        Image newQuadImg = gfm.loadImage(quadrantsImgNames.get(quad));
        Image quadImg = gfm.loadImage(quadrantsImgNames.get(quad),
                                     ((int)(newQuadImg.getWidth()*.25)),
                                     ((int)(newQuadImg.getHeight()*.24)));
       //System.out.println("quadImg size = " + (quadImg.getWidth() + " " + quadImg.getHeight()));
       this.setWidth(quadImg.getWidth());
       this.setHeight(quadImg.getHeight());
        
        
       //if(!isFirst)
        {
           // gc.scale(0.25, 0.24);
            isFirst = true;
        }
        gc.drawImage(quadImg, 0, 0);

        
        drawPlayersWithoutPlayer(quad,player);
        
        
        //ui.getMainPane().setCenter(null);
        StackPane pane = new StackPane();
        pane.getChildren().add(this);
        pane.setAlignment(Pos.TOP_CENTER);
        ui.getMainPane().setCenter(pane);
        
            }
    public void setjteState(Main.JTEPropertyType jteState) {
        this.jteState = jteState;
    }
    public JTEPropertyType getjtePropertytype()
    {
        return jteState;
    }
   /* public void ClearCanvas() {
        gamePane.getChildren().remove(gridRenderer);
        gridRenderer = new GridRenderer();
        gamePane.getChildren().addAll(gridRenderer);
    }*/
    public StackPane getSPane()
    {
        return pane;
    }
    public void drawPlayers(int quad) {
        ArrayList<Player> playersCurrentlyOnMap = gsm.findCurrentPlayersOn(quad+1);
        
        //gc.scale((this.getWidth()/.25), this.getHeight()/.24);
        for(Player player: playersCurrentlyOnMap)
        {
            Image playerPiece = player.getPlayerPiece();// // will be displayed on current picture
            
            Image playerFlag = player.getPlayerFlag();// will be displayed on hometown
            Position currentPosition = player.getCurrentCity().getPosition();
           // System.out.println("currentPosition = " + currentPosition);
            Position flagPosition = player.getHomeCity().getPosition();
            //System.out.println("flagPosition = " + flagPosition);
            gc.drawImage(playerPiece, (currentPosition.getX()-(playerPiece.getWidth()*.5)),
                    (currentPosition.getY()-playerPiece.getHeight())); 
            if(player.getHomeCity().getQuadrant() == (quad+1))
                gc.drawImage(playerFlag, (flagPosition.getX()-(playerFlag.getWidth())),
                    (flagPosition.getY()-playerFlag.getHeight()));
            
            double x = (currentPosition.getX()-(playerPiece.getWidth()*.5));
            double y = (currentPosition.getY()-playerPiece.getHeight());
            //Position currentPlayerPosition = new Position(x,y);
            if((quad+1) == gsm.getCurrentPlayer().getCurrentCity().getQuadrant())
                drawLinesForCurrentPlayer(gsm.getCurrentPlayer(),x);
            int destinationCityQuad = player.getNextDestion().getQuadrant();
            Position next = player.getNextDestion().getPosition();
            //if(destinationCityQuad == quad)
              //  gc.strokeOval(next.getX(), next.getY(), 25, 25);
        }
        
        
        /*this.setOnMouseClicked(e ->{
            System.out.println("e xpos and y = " + e.getX() + " " + e.getY());
        });*/
    }

    public void drawLinesForCurrentPlayer(Player player,double x) {
        double xPos = player.getPosition().getX();
        double yPos = player.getPosition().getY();
        
        for(City neighbor: player.getCurrentCity().getNeighbors())
            if(player.getCurrentCity().getQuadrant() == neighbor.getQuadrant())
                {
                    double newXpos = neighbor.getPosition().getX();
                    double newYpos = neighbor.getPosition().getY();
                    gc.strokeLine(xPos, yPos, newXpos, newYpos);
                }
            
        
    }

    public void moveAnimation(Player player, City selectedCity) {
        Lock lock = new ReentrantLock();
        Position oldPosition = player.getPreviousCity().getPosition();
        Position newPosition = selectedCity.getPosition();
        
        Image playerPiece = player.getPlayerPiece();
        
        DoubleProperty a = new SimpleDoubleProperty();
        DoubleProperty b = new SimpleDoubleProperty();
       
        Timeline timeline = new Timeline();
        timeline.setCycleCount(1);
        
        KeyValue oldX = new KeyValue(a,oldPosition.getX()-(playerPiece.getWidth()*.5));
        KeyValue oldY = new KeyValue(b,oldPosition.getY()-playerPiece.getHeight());
        
        KeyValue newX = new KeyValue(a,newPosition.getX()-(playerPiece.getWidth()*.5));
        KeyValue newY = new KeyValue(b,newPosition.getY()-playerPiece.getHeight());
        AnimationTimer timer = new AnimationTimer() {
                            @Override
                            public void handle(long now) {
                                //gc = getCanvas().getGraphicsContext2D();
                                repaintAnimation(player.getCurrentCity().getQuadrant()-1,player);
                                gc.drawImage(playerPiece, a.doubleValue(), b.doubleValue());
                                //ui.ClearCanvas();
                               // repaintAnimation(player.getCurrentCity().getQuadrant()-1,player);
                            }

            
                        };
        EventHandler onFinished = new EventHandler<ActionEvent>() {
            public void handle(ActionEvent t) {
                // might use this to clear the canvas and not draw player
                timer.stop();
                repaint(player.getCurrentCity().getQuadrant()-1);
                lock.unlock();
                //ui.ClearCanvas();
            }
        };
        
        KeyFrame keyFrame1 = new KeyFrame(Duration.seconds(0),
                                         oldX,oldY);
        
        KeyFrame keyFrame2 = new KeyFrame(Duration.seconds(1),
                                          onFinished,newX,newY);
       
        timeline.getKeyFrames().addAll(keyFrame1,keyFrame2);
                        lock.lock();
                        timer.start();
                        timeline.play();
                        
    }

    public void drawPlayersWithoutPlayer(int quad, Player currentPlayer) {
        
        ArrayList<Player> playersCurrentlyOnMap = gsm.findCurrentPlayersOn(quad+1);
        
        //gc.scale((this.getWidth()/.25), this.getHeight()/.24);
        for(Player player: playersCurrentlyOnMap)
        {
            if(!currentPlayer.equals(player)){
            Image playerPiece = player.getPlayerPiece();// // will be displayed on current picture
            Image playerFlag = player.getPlayerFlag();// will be displayed on hometown
            Position currentPosition = player.getCurrentCity().getPosition();
           // System.out.println("currentPosition = " + currentPosition);
            Position flagPosition = player.getHomeCity().getPosition();
            //System.out.println("flagPosition = " + flagPosition);
            gc.drawImage(playerPiece, (currentPosition.getX()-(playerPiece.getWidth()*.5)),
                    (currentPosition.getY()-playerPiece.getHeight())); 
            if(player.getHomeCity().getQuadrant() == (quad+1))
                gc.drawImage(playerFlag, (flagPosition.getX()-(playerFlag.getWidth())),
                    (flagPosition.getY()-playerFlag.getHeight()));
            
            double x = (currentPosition.getX()-(playerPiece.getWidth()*.5));
            double y = (currentPosition.getY()-playerPiece.getHeight());
            //Position currentPlayerPosition = new Position(x,y);
            if((quad+1) == gsm.getCurrentPlayer().getCurrentCity().getQuadrant())
                drawLinesForCurrentPlayer(gsm.getCurrentPlayer(),x);
            }
        }
        
    }

    public void drawFlightCard(Image flightCardImg) {
        gc = this.getGraphicsContext2D();
        gc.setStroke(Color.RED);
        
        this.setWidth(flightCardImg.getWidth());
        this.setHeight(flightCardImg.getHeight());
        
       // ui.ClearCanvas();
        
        gc.drawImage(flightCardImg,0,0);
        
        StackPane pane = new StackPane();
        pane.getChildren().add(this);
        pane.setAlignment(Pos.TOP_CENTER);
        ui.getMainPane().setCenter(pane);
        
    }

    public void drawDrag(Position pos) {
        double x = pos.getDX();
        double y = pos.getDY();
        ui.ClearCanvas();
        System.out.println("+"+x+ ","+y+")");
        repaint(pos);
    }

    public void repaint(Position pos) {
        if(gc == null)
        {
            gc = this.getGraphicsContext2D();
            gc.setStroke(Color.RED);
        }
        //gc.clearRect(0, 0, this.getWidth(), this.getHeight());
        ui.ClearCanvas();// may need ot change method
        Player player = ui.getGSM().getCurrentPlayer();
        int quad = player.getCurrentCity().getQuadrant()-1;
        
        PropertiesManager props = PropertiesManager.getPropertiesManager(); 
        quadrantsImgNames = props.getPropertyOptionsList(Main.JTEPropertyType.QUANDRANT_IMG_NAMES);
         
        Image newQuadImg = gfm.loadImage(quadrantsImgNames.get(quad));
        Image quadImg = gfm.loadImage(quadrantsImgNames.get(quad),
                                     ((int)(newQuadImg.getWidth()*.25)),
                                     ((int)(newQuadImg.getHeight()*.24)));
       //System.out.println("quadImg size = " + (quadImg.getWidth() + " " + quadImg.getHeight()));
       this.setWidth(quadImg.getWidth());
       this.setHeight(quadImg.getHeight());
       
       gc.drawImage(quadImg, 0, 0);
        
        
        //drawPlayers(quad);
       
        gc.drawImage(player.getPlayerPiece(), pos.getDX(), pos.getDY());
        
        //ui.getMainPane().setCenter(null);
        //ui.getMainPane().getChildren().remove(pane);
        pane.getChildren().add(this);
        pane.setAlignment(Pos.TOP_CENTER);
        ui.getMainPane().setCenter(pane);
        
    }
    
    
}
