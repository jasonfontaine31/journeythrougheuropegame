/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

/**
 *
 * @author Jason
 */
import JourneyThroughEurope.ui.GameFileManager;
import JourneyThroughEurope.ui.JourneyThroughEuropeUI;
import application.Main;
import application.Main.JTEPropertyType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.animation.AnimationTimer;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import properties_manager.PropertiesManager;

public class GameStateManager  {
    
    private JourneyThroughEuropeUI ui;
    
    private GameFileManager gfm;
    
    private ArrayList<Player> players;// = new ArrayList();
    private ArrayList<City> cities = new ArrayList();
    
    
    private Die die;
    
    private CardAction cardAction;
    
    private Deck redDeck,
                 greenDeck,
                 yellowDeck;
    
    private int currentPlayerTurn = 0;
    
    private final int cardsPerPlayer = 3;
    
    private String[] deckColors ={"red",//0
                                "green",//1
                                "yellow"};//2
    private City selectedCity;
    private final Comparator<City> compare = new CityComparator();
    private PriorityQueue<City> minHeap;
    private boolean isGameOver;
    private Dijkstra dx;
    
    
    public GameStateManager(JourneyThroughEuropeUI initUI)
    {
        ui = initUI;
        gfm = new GameFileManager(initUI);
        cities = initCities();
        players = new ArrayList();
        die = new Die();
        
    }
   
    public void initPlayers(HashMap<Integer,Boolean> isHuman, ArrayList<String> names)
    {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        ArrayList<String> playerPieceImgName = 
                props.getPropertyOptionsList(JTEPropertyType.PLAYERS_PIECES);
        ArrayList<String> playerColors = 
                props.getPropertyOptionsList(JTEPropertyType.FLAG_COLORS);
        ArrayList<String> flagImgNames =
                props.getPropertyOptionsList(JTEPropertyType.FLAG_NAMES);
        // method should cread a player using a for loop based on the information given from the hash map
        //Iterator it = isHuman.entrySet().iterator();
        // super(newName, newColor,newPlayerPiece,newPlayerFlag);
        for(int i = 0; i < names.size();i++){
            
            String newName = names.get(i);
            String newColor = playerColors.get(i);
            Image playerPiece = gfm.loadImage(playerPieceImgName.get(i),40,40);
            Image playerFlag = gfm.loadImage(flagImgNames.get(i),40,40);
            
             if(isHuman.get(i) == true)// when a player ishuman
                 addPlayer(new Human(newName,newColor,playerPiece,playerFlag));
             else
                 addPlayer(new AI(newName,newColor,playerPiece,playerFlag));
        }
        
    }
    
    public void addPlayer(Player newPlayer) {
       
       players.add(newPlayer);
       
    }

    private ArrayList<City> initCities(){
        
        
        return gfm.readFromCSVFile();
    }
    
    public Deck inItDeck(JTEPropertyType jteImgPath,
                        JTEPropertyType jteDecknames, String color){
        //read cardsImgNames from File from file
        Deck deck = new Deck(color);
        for(City city: cities)
        {
            if(city.getColor().equals(color))
            {
                //System.out.println("city = " + city.getName() +
                //                    " color = " + city.getColor());
                deck.addCardtoDeck(new Card(city.getColor(),city));
            }
        }
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        String imgPath = props.getProperty(jteImgPath);
        ArrayList<String> cardImgNames = props.getPropertyOptionsList(jteDecknames);
        
        for(String cardImgName: cardImgNames)
        {
            if(cardImgName.contains("_I.jpg"))// this is a back card
            {
                int end = cardImgName.lastIndexOf("_");
                String cityName = cardImgName.substring(0,end);
                Card card = findCard(deck,cityName);
                Image cardImg = gfm.loadImage(imgPath+cardImgName,191,272);
                card.setBackImg(cardImg);
              //                System.out.println("cityName = " + cityName +
                //      " color =" + card.getCity().getColor() + " back");
            }
            else
            {
               int end = cardImgName.lastIndexOf(".");
               String cityName = cardImgName.substring(0,end);
               Card card = findCard(deck,cityName);
               Image cardImg = gfm.loadImage(imgPath+cardImgName,191,272);
          //    System.out.println("cityName = " + cityName +
            //          " color =" + card.getCity().getColor() + " front");
               card.setFrontImg(cardImg);
            }
            
        }
        
        // check the different file type
        
        return deck;
       
    }

    public int roll() {
        return die.roll();
    }
    public void addPointstoPlayer(Player person,int points)
    {
        for(Player player: players)
            if(person == player){
                person.addPoints(points);
                person.setOriginPoint(points);
            }
    }
    public int getCurrentPlayerTurn()
    {
        return currentPlayerTurn;
    }
    public void nextTurn()
    {
            currentPlayerTurn =++currentPlayerTurn%players.size();
            getCurrentPlayer().setHasMoved(false);
        
    }
    public Player getCurrentPlayer()
    {
        return players.get(currentPlayerTurn);
    }

    public void findCitiesInQuad(int quad, Position pos) {
        
     minHeap = new PriorityQueue(compare);
        for(City city: cities)
        {
           if(city.getQuadrant() == quad) 
                if(distance( pos,city.getPosition() ) < 25){
                    //System.out.println(city.getName());
                    minHeap.add(city);
                    //ui.setCityLabel(city.getName());
                    
                }
        }
        if(!minHeap.isEmpty())
        {
            City city = minHeap.peek();
            ui.setCityLabel(city.getName());
            setSelectedCity(city);
            //System.out.print(city.getName() + ": ");
            //System.out.println(city.getName()+" " + city.getQuadrant());
           // System.out.println("");
            //System.out.println("city position = " + city.getPosition());
        }
    }
    public double distance(Position newPos, Position oldPos) {

        return oldPos.computeDistance(newPos);
        //return Math.sqrt((ydelta*ydelta)+(xdelta*xdelta));
    }
    
    public void inItPlayersHomeTown(Player player, City city){
        player.setHomeCity(city);
        player.setCurrentCity(city);
        player.setPreviousCity(city);
    }

    public Image getCurrentPlayerImg() {
        return getCurrentPlayer().getPlayerPiece();
    }

    public City findCity(String cityName) {
        City newCity = null;
        for(City city: cities)
            if(city.getName().equals(cityName)){
                newCity = city;
                break;
            }
             return newCity;
        
    }

    private Card findCard(Deck deck, String cityName) {
        return deck.findCard(cityName);    
    }

    public void startNewGame() {
        setGameStart();
        
        redDeck = 
                inItDeck(JTEPropertyType.RED_PATH,JTEPropertyType.RED_DECK,deckColors[0]);// yellow cards
        greenDeck =
                inItDeck(JTEPropertyType.GREEN_PATH,JTEPropertyType.GREEN_DECK,deckColors[1]);
        yellowDeck = 
                inItDeck(JTEPropertyType.YELLOW_PATH,JTEPropertyType.YELLOW_DECK,deckColors[2]);
        
        
        shuffleDecks();
        // each person gets a card at random
        dealCardstoPlayers();
        // sets a random hometown for the players
        setPlayersHometown();
        //initialize city neightbors
        initNeighbors();
        //checkNeighbors();
        initFlightNeighbors();
        inItVertices();

        //printCities();
        //printCurrentDestinations();
       
        
    }

    public void shuffleDecks() {
      redDeck.shuffle();
      yellowDeck.shuffle();
      greenDeck.shuffle();
    }
    
    public Card getaCard()
    {
        return redDeck.takeCard();
    }

    private void dealCardstoPlayers() {
        // this method should take how many players a
        int outerSize = players.size();
        // innerSize would be cards perPlayer
        int num =0;

        for(int j=0;j<cardsPerPlayer;j++)// cards given to them
           for(int i = 0;i < outerSize;i++)// player getting the 
           {
               int pos = num++%3;
               Player player = players.get(i);
              String color = deckColors[pos];
              switch(color)
              {
                  case "red":
                  player.giveCard(j, redDeck.takeCard());
                  break;
                  case "yellow":
                  player.giveCard(j, yellowDeck.takeCard());
                  break;
                  case "green":
                  player.giveCard(j, greenDeck.takeCard());
                  break;
              }
               
           }
       
        
    }

    public void setPlayerPoints(int points) {
        getCurrentPlayer().addPoints(points);
    }
    public void setCurrentPlayerTurn(int turn)
    {
        currentPlayerTurn = turn;
    }
    public void setPlayersHometown() {
        int size = players.size();
        
        for(int i = 0; i < size; i++)
        {
          City homeCity = players.get(i).cards.get(0).getCity();
          inItPlayersHomeTown(players.get(i),homeCity);
         // grab random s  
        }
        
    }

    private void printCities() {
        for(Player player: players)
        {
            System.out.println("player home town = " + player.origin.getName() + " " + player.origin.getQuadrant());
        }
    }

    public void initNeighbors() {
        gfm.initNeighbors(cities);
    }

    public ArrayList<Player> findCurrentPlayersOn(int quadrant) {
        ArrayList<Player> playersOnQuadrant = new ArrayList();
        
        for(Player player: players)
        {
            if(player.currentCity.getQuadrant() == quadrant)
                playersOnQuadrant.add(player);
        }
        return playersOnQuadrant;
    }

    public void movePlayertoCity() {
        Player player = getCurrentPlayer();
        if(selectedCity != null)
          if(player.getCurrentCity().isCityNeighbor(selectedCity)){
              if(isCityOccupied(player))// && !isCityPrevious(player,selectedCity))
              //if(player.canMove())
                {
                    /*for(City city: player.getCurrentCity().getNeighbors())
                    {
                        System.out.println("city = " + city.getName());
                    }*/
                  if(!player.getCurrentCity().isSeaRoute(selectedCity))
                  {
                  player.addPoints(-1);
                    if(player.getPoints() > 0)
                        ui.updateDieImgView(player.getPoints()-1);
                    movetoCity(player,selectedCity);
                    
                     ui.getBoardRenderer().moveAnimation(player,selectedCity);
                  }
                  else
                  {
                      if(!player.hasMoved)
                      {
                                player.addPoints(-1);
                         if(player.getPoints() > 0)
                             ui.updateDieImgView(player.getPoints()-1);
                         movetoCity(player,selectedCity);

                          ui.getBoardRenderer().moveAnimation(player,selectedCity);
                      }
                      else
                          nextTurn();
                  }
                     
                   
                     // player is assumed to alreayd have his selected city with value
                    
                    // this is where I would put the animatio
                    //ui.getBoardRenderer().repaint(((selectedCity.getQuadrant()-1)));
                }
              
          }
        
              
              
            
    }

    public void setSelectedCity(City city) {
        selectedCity = city;
    }

    public void checkNeighbors() {
        for(City city: cities){
            System.out.print(city.getName() +"");
            for(City neighbor : city.getNeighbors())
            {
                System.out.print(", " + neighbor.getName());
            }
        System.out.println();
        
            
                
    }
    }

    public boolean isCityOccupied(Player player) {
        for(Player otherPlayer : players)
            if(!otherPlayer.equals(player))
                if(player.getCurrentCity().equals(otherPlayer.getCurrentCity()))
                    return false;
        return true;
    }

    public City findCitiesOnFlightMap(Position flightCoords) {
         minHeap = new PriorityQueue(compare);
        for(City city: cities)
        {
            if(city.isAirPort())
                if(distance( flightCoords,city.getFlightCoordinates() ) < 20){
                    //System.out.println(city.getName());
                    minHeap.add(city);
                    //ui.setCityLabel(city.getName());
                   // System.out.println("city position = " + city.getPosition());
                }
        }
         if(!minHeap.isEmpty())
        {
            City city = minHeap.peek();
            //ui.setCityLabel(city.getName());
            //setSelectedCity(city);
            //System.out.print(city.getName() + ": ");
            //System.out.println(city.getName()+" " + city.getQuadrant());
            //System.out.println(city.getName());
            return city;
        }
         return null;
    }
    public void initFlightNeighbors() 
    {
        for(City city: cities)
        {
            int flightQuad = city.getFlightQuadrant();
            for(City otherCity: cities)
            {
                int currentQuad = otherCity.getFlightQuadrant();
                    if(flightQuad != currentQuad)
                    {
                    if(currentQuad == 1)
                { 
                    if(flightQuad == 2 || flightQuad == 4)
                        city.addFlightNeighbor(city, false);
                }
                else if(currentQuad == 2)
                {
                    if(flightQuad == 1 || flightQuad == 3)
                        city.addFlightNeighbor(city, false);
                }
                else if(currentQuad == 3)
                {
                    if(flightQuad == 2 || flightQuad == 4 || flightQuad == 6)
                        city.addFlightNeighbor(city, false);
                }
                else if(currentQuad == 4)
                {
                    if(flightQuad == 1 || flightQuad == 3 || flightQuad == 5)
                        city.addFlightNeighbor(city, false);
                }
                else if(currentQuad == 5)
                {
                    if(flightQuad == 4 || flightQuad == 6)
                        city.addFlightNeighbor(city, false);
                }
                else if(currentQuad == 6)
                {
                    if(flightQuad == 3 || flightQuad == 5)
                        city.addFlightNeighbor(city, false);
                }
                    }
                else
                {
                    city.addFlightNeighbor(city, true);// true for within section
                }
                
            }
        }
    }
    public void flightMove(Position position) {
        Player player = getCurrentPlayer();
        City flightCity = findCitiesOnFlightMap(position);// we want to go to this city
        City currentCity = player.getCurrentCity();// current at this city
        
        int flightQuad = flightCity.getFlightQuadrant();
        int currentQuad = currentCity.getFlightQuadrant();
        // test cases of moving to different quadrants
        if(flightQuad != currentQuad){
        if(player.getPoints() > 3)
        {
            if(currentQuad == 1)
            { 
                if(flightQuad == 2 || flightQuad == 4)
                    movetoCity(player,flightCity);
            }
            else if(currentQuad == 2)
            {
                if(flightQuad == 1 || flightQuad == 3)
                    movetoCity(player,flightCity);
            }
            else if(currentQuad == 3)
            {
                if(flightQuad == 2 || flightQuad == 4 || flightQuad == 6)
                    movetoCity(player,flightCity);
            }
            else if(currentQuad == 4)
            {
                if(flightQuad == 1 || flightQuad == 3 || flightQuad == 5)
                    movetoCity(player,flightCity);
            }
            else if(currentQuad == 5)
            {
                if(flightQuad == 4 || flightQuad == 6)
                    movetoCity(player,flightCity);
            }
            else if(currentQuad == 6)
            {
                if(flightQuad == 3 || flightQuad == 5)
                    movetoCity(player,flightCity);
            }
            player.addPoints(-4);
            ui.updateDieImgView(player.getPoints()-1);
        }
        }
        else// flight is withint the same quadrant
        {
            movetoCity(player,flightCity);
            player.addPoints(-2);
            ui.updateDieImgView(player.getPoints()-1);
        }
        ui.ClearCanvas();
        ui.getBoardRenderer().repaint(flightCity.getQuadrant()-1);
    }

    /*public boolean isCityPrevious(Player player,City selectedCity) {
        if(selectedCity == player.getPreviousCity())
            return true;
        return false;
    }*/

    public void movetoCity(Player player,City selectedCity) {
        player.setPreviousCity(player.getCurrentCity());
                    player.addHistory(player.getCurrentCity(),player.getPoints()+1);
                    player.setCurrentCity(selectedCity);
                    Card cardtoRemove = player.getplayerDestinationCard();
                    if(cardtoRemove == null){
                        
                    }
                    else{
                        removeCardfromLeftPane(cardtoRemove.getFrontImg());
                        addCardBacktoDeck(cardtoRemove);
                        player.addPoints(-player.getPoints());
                        ui.sethasCardRemove(true);
                        ui.update();
                        
                    }
                    //player.setHasMoved(true);
    }
    public void removeCardfromLeftPane(Image cardImg)
    {
        ui.removeCardfromLeftPane(cardImg);
    }
    public void addCardBacktoDeck(Card cardtoRemove) {
        switch(cardtoRemove.getColor())
        {
            case "red":
                redDeck.addCardtoDeck(cardtoRemove);
                break;
            case "green":
                greenDeck.addCardtoDeck(cardtoRemove);
                break;
            case "yellow":
                yellowDeck.addCardtoDeck(cardtoRemove);
                break;
                
                
        }
    }
    
    public void printCurrentDestinations() {
        for(Player player: players)
        {
            System.out.println(player.getName() + " current Destination is " + player.getNextDestion().getName());
        }
    }

    public boolean gameStillinProgress() {
        if(getCurrentPlayer().getCardsAmount() == 0)
            isGameOver = true; 
        return isGameOver;
    }

    public void setGameStart() {
        isGameOver = false;
    }
    public boolean getIsGameOver()
    {
        return isGameOver;
    }

    public void endGame() {
        ui.endGame();
    }

    public ArrayList<Player> getAllPlayers() {
        return players;
    }

    public boolean isHistoryEmpty() {
        for(Player player : players)
            if(player.getHistory().isEmpty())
                return true;
            return false;
    }
    public void endTurn()
    {
        
    }
    public void loadNewGame() {
        setGameStart();
        
        redDeck = 
                inItDeck(JTEPropertyType.RED_PATH,JTEPropertyType.RED_DECK,deckColors[0]);// yellow cards
        greenDeck =
                inItDeck(JTEPropertyType.GREEN_PATH,JTEPropertyType.GREEN_DECK,deckColors[1]);
        yellowDeck = 
                inItDeck(JTEPropertyType.YELLOW_PATH,JTEPropertyType.YELLOW_DECK,deckColors[2]);
        
        
        shuffleDecks();
        initNeighbors();
        inItVertices();
        
    }
    public Card findCardfromDecks(String cardName)
    {
        Card red = redDeck.findCard(cardName);
        Card green = greenDeck.findCard(cardName);
        Card yellow = yellowDeck.findCard(cardName);
        Card toReturn = null;
        while(toReturn == null)
        {
            toReturn = red;
            toReturn = green;
            toReturn = yellow;
        }
         return toReturn;
        
    }
        
    public void inItVertices() {
        ArrayList<Vertex> vertices = new ArrayList();
        // first initiliaze sea and land weight
        for(City city: cities)
        {
            vertices.add(new Vertex(city));
        }
        // initialize neightbors for land
        for(Vertex v : vertices)
            for(City neighbor : v.city.getNeighbors())
            {
                Vertex u = findVertex(vertices,neighbor);
                if(v.city.isSeaRoute(neighbor))
                    v.adjacencies.add(new Edge(u,City.seaWeight));
                else
                    v.adjacencies.add(new Edge(u,City.landWeight));
                
                    
            }
        
        // intiliaze neighbors for Flight
        /*for(Vertex v : vertices)
           for(City city : cities)
           {
               Vertex u = new Vertex(city);
               if(v.city.isFlightRouteinSec(city) == null)
               {
                   continue;
               }
               else if(v.city.isFlightRouteinSec(city))// if it is true then it is in the section
                   v.adjacencies.add(new Edge(u,City.flightinSection));
               else
                   v.adjacencies.add(new Edge(u,City.flightoutSection));
           }*/
        setVerticestoAI(vertices);
       
    }
    public void setVerticestoAI(ArrayList<Vertex> vertices) {
        for(Player player: players)
            if(player instanceof AI)
                {
                    AI ai = (AI) player;
                    ai.setVertices(vertices);
                }
    }

    private Vertex findVertex(ArrayList<Vertex> vertices, City neighbor) {
        for(Vertex v : vertices)
            if(v.city == neighbor)
                return v;
        return null;
    }

    public void processComputerMove() {
        try {
            Player player = getCurrentPlayer();
            ArrayList<City> cities = new ArrayList();
            int index = 1;
            
            // index = 0;
            
            //Thread.sleep(1000);
            while(!player.isTurnOver())
            {
                if(cities.isEmpty())
                {
                    cities = player.processTurn();
                    index = 0;
                }
                City city = cities.remove(index++);
                System.out.println("city DEs = " + city.getName());
                selectedCity = city;
                Thread.sleep(1000);
                movePlayertoCity();
                Thread.sleep(1000);
                
                int points = player.getPoints();
                player.addPoints(-points);
                if(player.getOriginPoints() != 6)
                    nextTurn();
                ui.sethasCardRemove(false);
                /*try {
                Thread.sleep(100);
                
                } catch (InterruptedException ex) {
                Logger.getLogger(GameStateManager.class.getName()).log(Level.SEVERE, null, ex);
                }*/
            }
            Thread.sleep(1000);
            ui.update();
        } catch (InterruptedException ex) {
            Logger.getLogger(GameStateManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    public Card findCardfromDecks(String cardName, String color) {
        Card card = null;
        switch(color)
        {
            case "red":
               card = redDeck.findCard(cardName);
               redDeck.remove(card);
                break;
            case "green":
               card = greenDeck.findCard(cardName);
               greenDeck.remove(card);
                break;
            case "yellow":
                card = yellowDeck.findCard(cardName);
                yellowDeck.remove(card);
                break;
                
        }
        return card;
        
    }

    public void setPlayers(ArrayList<Player> players) {
        this.players = players;
    }
    
}
class CityComparator implements Comparator<City>
{
    @Override
    public int compare(City o1, City o2) {
        if(o1.getPosition().getDistanceFromCity() < o2.getPosition().getDistanceFromCity())
            return -1;
        if(o1.getPosition().getDistanceFromCity() > o2.getPosition().getDistanceFromCity())
            return 1;
        return 0;
        
    }
}
