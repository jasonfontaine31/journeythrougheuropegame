/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import java.util.ArrayList;
import java.util.HashMap;
import javafx.scene.shape.Circle;

/**
 *
 * @author Jason
 */




public class City {
    
    private boolean// isAirport,
                    hasVisited;
    
    private int airRegion,
                quadrant;
    
    private Position position,
                     flightCardPosition;
    
    private HashMap<City, Boolean> isCitySeaRoute,//;// uses city as returns true or false value if it is a see route
                                   flightNeighbors;
    private ArrayList<City> neighbors;
    
    private String cityDescription;
    
    private String name,
                   color;
    
    private int flightQuad;
    
    public final static int landWeight = 1,
                      seaWeight = 6,
                      flightinSection = 2,
                      flightoutSection = 4;
    
    
    public City(String newName, String newColor, int newQuadrant, Position newPosition)
                //HashMap<Boolean, City> newNeighbors,Circle newCircle, boolean hasAirport,
                 //Position newPosition)
    {
        //isAirport = hasAirport;
        //cityDescription = newCityDescript;
        //neighbors = newNeighbors;
        //circle = newCircle;
        name = newName;
        color = newColor;
        quadrant = newQuadrant;
        position = newPosition;
        isCitySeaRoute = new HashMap();
        neighbors = new ArrayList();
        flightNeighbors = new HashMap();
        hasVisited = false;
   
    }
    public City(String newName, String newColor, int newQuadrant, 
                Position newPosition,Position newFlightCardPosition,int newFlightQuad)
    {
        this(newName,newColor,newQuadrant,newPosition);
        flightCardPosition = newFlightCardPosition;
        flightQuad = newFlightQuad;
        
    }
    public boolean isAirPort()
    {
        return (!(flightCardPosition == null));
    }
    public void addNeighbor(City city,boolean isSeaRoute)
    {
        isCitySeaRoute.put(city, isSeaRoute);
        neighbors.add(city);
    }
    public void addFlightNeighbor(City city, boolean value)
    {
        flightNeighbors.put(city,value);
    }
    public boolean isSeaRoute(City city)
    {
        return isCitySeaRoute.get(city);
            //return null;
    }
    public int getQuadrant()
    {
        return quadrant;
    }
    public int getFlightQuadrant()
    {
        return flightQuad;
    }
    public ArrayList<City> getNeighbors()
    {
        return neighbors;
    }
    public HashMap<City,Boolean> getFlightNeighbor()
    {
        return flightNeighbors;
    }
    public Boolean isFlightRouteinSec(City key)
    {
        return flightNeighbors.get(key);
    }
    public Position getPosition()
    {
        return position;
    }
    
    public String getName()
    {
        return name;
    }
    public String getColor()
    {
        return color;
    }
    public boolean isCityNeighbor(City selectedCity) {
        boolean isNeighbor = false;
        for(City city: neighbors)
            if(city.equals(selectedCity))
                isNeighbor = true;
          return isNeighbor;  
    }

    public Position getFlightCoordinates() {
        return flightCardPosition;
    }
    
}
