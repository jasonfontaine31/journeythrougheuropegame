/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jason
 */
public class Die {
    
    private int numberOnDie;
    
    public Die()
    {
        numberOnDie = 0;
    }

   public int roll() {
        return numberOnDie = (int)(Math.random()*6);
    }
    
}
