/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Jason
 */
public class Card {
    
    //private GameStateManager gsm;
    
    private boolean isSpecial;
    private String color;
    private int carAction;
    
    private Image frontImg,
                  backImg;
    private ImageView frontImgView,
                      backImgView;
    
    private City city;
    
    public Card(String newColor, City newCity)
    {
        //gsm = newGSM;
        color = newColor;
        //frontImg = new Image();//newFront;
        //frontImg = new Image();
        //backImg = new Image();//newBack; card might not have a back image
        city = newCity;
    }
    
    public City getCity()
    {
        return city;
    }
    public Image getFrontImg()
    {
        return frontImg;
    }
    public String getColor()
    {
        return color;
    }
    public Image getBackImg()
    {
        return backImg;
    }
    public void setFrontImg(Image newFrontImg)
    {
        frontImg = newFrontImg;
        
    }
    public void setBackImg(Image newBackImg)
    {
        backImg = newBackImg;
    }
    
}
