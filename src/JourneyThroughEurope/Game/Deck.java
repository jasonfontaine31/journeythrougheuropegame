/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Jason
 */
public class Deck {
    
    private ArrayList<Card> cards;
    
    private String color;
    
    public Deck(String newColor)
    {
        cards = new ArrayList();
        color = newColor;
    }
    
    public Card takeCard()
    {
        if(!(cards.isEmpty()))
               return  cards.remove(0);// removes card from top of deck
        return null;
    }
    public void shuffle()
    {
        Collections.shuffle(cards);
        Collections.shuffle(cards);
        Collections.shuffle(cards);
    }
    public void addCardtoDeck(Card card)
    {
        cards.add(card);
    }
    public Card findCard(String cityName) {
        Card newCard = null;
        if(cityName.contains("_"))
           cityName = cityName.replace("_", " ");
        for(Card card: cards)
            if(card.getCity().getName().equals(cityName))
            {
                newCard = card;
                break;
            }
        return newCard;
    }

    public void remove(Card card) {
        cards.remove(card);
    }
    
    
    
    
}
