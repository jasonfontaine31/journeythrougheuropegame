/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import javafx.scene.image.Image;

/**
 *
 * @author Jason
 */
public abstract class Player {
    
    protected String name;
    protected ArrayList<String> history;
    protected City origin;
    protected String color;
    
    protected int points = 0,
                  originPoints;
    
    protected HashMap<Integer, Card> cards; 
    
    protected City nextDestination,
                   currentCity,
                   previousCity;
    
    protected Image playerPiece,
                    playerFlag;
    protected boolean hasMoved;
    
    
    
    public Player(String newName, String newColor,Image newPlayerPiece,Image newPlayerFlag){
        name  = newName;
        playerPiece = newPlayerPiece;
        playerFlag = newPlayerFlag;
        color = newColor;
        cards = new HashMap();
        history = new ArrayList();
        hasMoved = false;
    }
    
    public void setPlayerPiece(Image newPlayerPiece)
    {
        playerPiece = newPlayerPiece;
    }
    public void setHasMoved(boolean value)
    {
        hasMoved = value;
    }
    public boolean getHasMoved()
    {
        return hasMoved;
    }
    public void setPlayerFlag(Image newPlayerFlag)
    {
        playerFlag = newPlayerFlag;
    }
    public Image getPlayerPiece()
    {
        return playerPiece;
    }
    public void setPreviousCity(City city)
    {
        previousCity = city;
    }
    public City getPreviousCity()
    {
        return previousCity;
    }
    public Image getPlayerFlag()
    {
        return playerFlag;
    }
    public void setHomeCity(City city){
        origin = city;
    }
    public City getHomeCity()
    {
        return origin;
    }
    public void setCurrentCity(City city)
    {
        currentCity = city;
    }
    public City getCurrentCity()
    {
        return currentCity;
    }
    public void addPoints(int morePoints)
    {
        points += morePoints;
    }
    public void setOriginPoint(int points)
    {
        originPoints = points;
    }
    public int getOriginPoints()
    {
        return originPoints;
    }
    public Position getPosition()
    {
        return currentCity.getPosition();
    }   
    public void giveCard(int newNum, Card newCard)
    {
        cards.put(newNum, newCard);
        setCurrentDestination(newCard.getCity());
    }
    public String getName()
    {
        return name;
    }
    public boolean canMove()
    {
        return (points > 0);
    }
    /*public void printHistory()
    {
        for(City city : history)
        {
            System.out.println(city.getName() + " ");
        }
        System.out.println("");
    }*/
    public abstract ArrayList<City> processTurn();

    public HashMap<Integer, Card> getCards() {
        return cards;
    }
    public void setCurrentDestination(City city)
    {
        nextDestination = city;
    }
    public void setNextDestination()
    {
        for(int i = 0; i < cards.size();i++)
        {
          nextDestination = cards.get(i).getCity();
        }
        
    }
    public void addHistory(City city,Integer point)
    {
        String newHistory = name +  " rolled a " + point + " is currently on " + city.getName();
        
        history.add(newHistory);
    }
    public ArrayList<String> getHistory()
    {
        return history;
    }
    public int getPoints()
    {
        return points;
    }
    public boolean isTurnOver() {
        return !(points > 0);
    }

    public Card getplayerDestinationCard() {
        if(currentCity == nextDestination)
            for(int i = 0; i < cards.size();i++)
            {
                if(cards.get(i).getCity() == nextDestination)
                {
                    // removes that card at that position

                    return cards.remove(i);
                }
            }
        return null;
    }

    public City getNextDestion() {
       return nextDestination;
    }

    int getCardsAmount() {
       return cards.size();
    }

    public String printFormat() {
        // this is thr pr
        String format = "";
        // should print like this
        //name,color,humanorAI,history in parethesis,orgin,nextDestination,currentCity,
        //previousDestination,//saveCardsformatwouldbeIntthanCity
        format+=(name +","+color+","+points +","+isPlayerHuman() + "," + formatCities()+ 
                 saveCards());// + formatHistory());
        return format;
    }
    public boolean isPlayerHuman()
    {
        return (this instanceof Human);
    }
    public String formatHistory() {
        String historyPrint = "";
        if(!history.isEmpty())
            for(String hist : history)
            {   
                historyPrint+=",$" + hist;
            }
        //historyPrint+="";
        return historyPrint;
    }

    private String formatCities() {
        String cities = "";//origin,next,current,previous
        cities+=origin.getName()+","+nextDestination.getName()+","+
                currentCity.getName() + "," + previousCity.getName();
        return cities;
    }

    private String saveCards() {
        String format = "";
        for(int i = 0; i < cards.size(); i++)
            format+=","+ i + "," + cards.get(i).getCity().getName() +
                    "," + cards.get(i).getCity().getColor();
        return format;
    }
    
    
    
}
