/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javafx.scene.image.Image;

/**
 *
 * @author Jason
 */
public class AI extends Player{
    
    //Iterator it;
    ArrayList<Vertex> next;
    private ArrayList<Vertex> vertices;
    public  AI(String newName, String newColor,Image newPlayerPiece,
            Image newPlayerFlag)
    {
        super(newName, newColor,newPlayerPiece,newPlayerFlag);
        next = new ArrayList();
        //this.dx = dx;
    }

    public void setVertices(ArrayList<Vertex> newVertices)
    {
        vertices = newVertices;
    }
    @Override
    public ArrayList<City> processTurn() {
        
     Dijkstra.computePaths(vertices.get(findCurrentVertexPos()));
        System.out.println(currentCity.getName());
     next = (ArrayList<Vertex>)
             (Dijkstra.getShortestPathTo(vertices.get(findNextVertexPos())));
     System.out.println(nextDestination.getName());
     
     //Iterator it = next.iterator();// should be a vertex
     ArrayList<City> cities = new ArrayList();
     for(Vertex v : next)
     {
         System.out.print(v.city.getName() + " ");
         cities.add(v.city);
         
     }
        cities.remove(currentCity);
        System.out.println("");
     ///Vertex v = (Vertex) next.get(1);
     return cities;
    }
    public int findCurrentVertexPos()
    {
        for(int i = 0; i < vertices.size(); i++)
            if(vertices.get(i).city == currentCity)
                return i;
        return -1;
    }
    public int findNextVertexPos()
    {
        for(int i = 0; i < vertices.size(); i++)
            if(vertices.get(i).city == nextDestination)
                return i;
        return -1;
    }
    
}
