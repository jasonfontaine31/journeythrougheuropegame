/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JourneyThroughEurope.Game;

/**
 *
 * @author Jason
 */
public class Position {

        private int x;
        private int y;
        private double distanceFromCity;
        private double doubleX;
        private double doubleY;

        public Position(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public Position(double x,double y)
        {
            doubleX = x;
            doubleY = y;
        }
        public double getDX()
        {
           return doubleX;
        }
        public int getX(){
            return x;
        }
        public double getDY()
        {
            return doubleY;
        }
        public int getY() {
            return y;
        }
        public double computeDistance(Position pos)
        {
            int y2 = getY(),
            x2 = getX(),
            y1 = pos.getY(),
            x1 = pos.getX();
        //int xdelta, ydelta;
        int ydelta =y2-y1;
        int xdelta =x2-x1;
        distanceFromCity = Math.sqrt((ydelta*ydelta)+(xdelta*xdelta));
        return distanceFromCity;
        }
        @Override
        public String toString()
        {
            String posStr = "X_position = " + x + "\n";
            posStr+= "Y_Position = " + y + "\n";
            return posStr;
        }
        public double getDistanceFromCity()
        {
            return distanceFromCity;
        }
    }
